import java.util.Random;

public class Die{
    private int pips;
    private Random rand = new Random();

    public Die(){
        this.pips = 1;

        Random rand = new Random();
    }

    public int getPips(){
        return this.pips;
    }

    public int roll(){
        this.pips = rand.nextInt(5) + 1;
        return this.pips;
    }

    @Override
    public String toString() {
        return "" + this.getPips();
    }
}
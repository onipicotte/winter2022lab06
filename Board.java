public class Board {
    private Die d1 = new Die();
    private Die d2 = new Die();
    private boolean[] closedTiles;

    public Board() {
        d1.roll();
        d2.roll();

        closedTiles = new boolean[12];
    }

    public boolean playATurn() {
        d1.roll();
        d2.roll();

        int total = d1.getPips() + d2.getPips();
        
        System.out.println("You rolled a " + d1 + " and a " + d2);

        if (closedTiles[total - 1] == false) {
            closedTiles[total - 1] = true;
            System.out.println("Closing tile: " + total);
            return false;
        } else {
            System.out.println("The tile " + total + " is already shut.");
            return true;
        }
    }

    @Override
    public String toString() {
        String boxes = "";

        for (int i = 1; i <= closedTiles.length; i++) {
            if (closedTiles[i - 1] == false) {
                boxes = boxes + i + " ";
            } else {
                boxes += "X ";
            }
        }

        return boxes;
    }
}

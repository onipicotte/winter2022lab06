import java.util.Scanner;

public class ShutTheBox {
    public static void main(String[] args) {
        Scanner key = new Scanner(System.in);
        boolean gameOver = false;
        int p1Win = 0;
        int p2Win = 0;
        boolean session = true;
        String procede = "";

        System.out.println("Welcome to Shut the Box.");
        System.out.println("Made by Oni Picotte");
        
        while (session == true) {
            Board game = new Board();

            while (gameOver != true) {
                if (gameOver == false) {
                    System.out.println("\nPlayer 1's turn");
                    System.out.println(game);
    
                    if (game.playATurn() == true) {
                        System.out.println("Player 2 wins!");
                        procede = "";
                        p2Win++;
                        System.out.println("\nThe player 1 has " + p1Win + " points and the player 2 has " + p2Win + " points");
                        gameOver = true;
                    }
                }
    
                if (gameOver == false) {
                    System.out.println("\nPlayer 2's turn");
                    System.out.println(game);
    
                    if (game.playATurn() == true) {
                        System.out.println("Player 1 wins!");
                        procede = "";
                        p1Win++;
                        System.out.println("\nThe player 1 has " + p1Win + " points and the player 2 has " + p2Win + " points");
                        gameOver = true;
                    }
                }
            }

            switch (procede) {
                case "":
                    System.out.println("\nIf you want to play again type \"yes\". If you want to stop type \"no\".");
                    procede = key.nextLine();
                    break;
                case "yes":
                    System.out.println("\nBeginning a new game...");
                    gameOver = false;
                    break;
                case "no":
                    System.out.println("\nThanks for playing Shut the Box.");
                    session = false;
                    break;
                default:
                    System.out.println("\nPlease enter \"yes\" or \"no\".");
                    procede = key.nextLine();
                    break;
            }
        }
        key.close();
    }
}
